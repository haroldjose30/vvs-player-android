package br.com.wisereducacao.playertest

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import br.com.wisereducacao.vvsplayer.WiserPlayerFragment

class MainActivity : AppCompatActivity() {

    private val urlVideo = "http://blueappsoftware.in/layout_design_android_blog.mp4"
    private var player_fragment: WiserPlayerFragment? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        player_fragment = supportFragmentManager.findFragmentById(R.id.wiserPlayerFragment2) as WiserPlayerFragment?
        if (player_fragment != null)
            player_fragment?.SetupPlayer(urlVideo,0,true)

        //todo: harold - ver como utilizar a legenda
    }
}
