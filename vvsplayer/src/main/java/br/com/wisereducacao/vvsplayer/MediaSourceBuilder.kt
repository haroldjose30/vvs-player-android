package br.com.wisereducacao.vvsplayer


import android.content.Context
import android.net.Uri
import com.google.android.exoplayer2.C
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.source.dash.DashMediaSource
import com.google.android.exoplayer2.source.dash.DefaultDashChunkSource
import com.google.android.exoplayer2.source.hls.DefaultHlsDataSourceFactory
import com.google.android.exoplayer2.source.hls.HlsMediaSource
import com.google.android.exoplayer2.source.smoothstreaming.DefaultSsChunkSource
import com.google.android.exoplayer2.source.smoothstreaming.SsMediaSource
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory
import com.google.android.exoplayer2.util.Util


class MediaSourceBuilder {

    //Build various MediaSource depending upon the type of Media for a given video/audio uri
    fun build(context:Context?,uriString: String): MediaSource {
        val userAgent =  "Wisereducacao-Android-Player"

        var uri = Uri.parse(uriString)

        //se for um arquivo local
        if (!uriString.contains("http",false)) {
            val dataSourceFactory = DefaultDataSourceFactory(context,userAgent)
            return ProgressiveMediaSource.Factory(dataSourceFactory).createMediaSource(uri)
        }

        //se for um link
        val defaultHttpDataSourceFactory = DefaultHttpDataSourceFactory(userAgent)
        when (val type: Int = Util.inferContentType(uri)) {

            C.TYPE_DASH -> {
                val dashChunkSourceFactory = DefaultDashChunkSource.Factory(defaultHttpDataSourceFactory)

                return DashMediaSource.Factory(dashChunkSourceFactory, defaultHttpDataSourceFactory)
                    .createMediaSource(uri)

            }

            C.TYPE_SS -> {
                val ssChunkSourceFactory = DefaultSsChunkSource.Factory(defaultHttpDataSourceFactory)

                return SsMediaSource.Factory(ssChunkSourceFactory, defaultHttpDataSourceFactory)
                    .createMediaSource(uri)
            }

            C.TYPE_HLS -> {

                val hlsDataSourceFactory = DefaultHlsDataSourceFactory(defaultHttpDataSourceFactory)

                return HlsMediaSource.Factory(hlsDataSourceFactory)
                    .createMediaSource(uri)

            }

            C.TYPE_OTHER -> {
                return ProgressiveMediaSource.Factory(defaultHttpDataSourceFactory)
                    .createMediaSource(uri)
            }


            else -> {
                throw IllegalStateException("Unsupported type: $type")
            }

        }
    }
}