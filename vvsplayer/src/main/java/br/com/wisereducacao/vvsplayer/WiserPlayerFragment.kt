package br.com.wisereducacao.vvsplayer

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.google.android.exoplayer2.*
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.source.TrackGroupArray
import com.google.android.exoplayer2.trackselection.*
import com.google.android.exoplayer2.ui.PlayerView
import com.google.android.exoplayer2.upstream.BandwidthMeter
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter
import kotlinx.android.synthetic.main.fragment_wiserplayer.*
import kotlinx.android.synthetic.main.fragment_wiserplayer.view.*
import java.net.CookieHandler
import java.net.CookieManager
import java.net.CookiePolicy


class WiserPlayerFragment(uriString:String = "", resumePosition: Long = 0,playWhenReady:Boolean = true) : Fragment() {

    /**
     * Listeners para expor os eventos do player externamente
     */
    var PlayerEventsListener: IPlayerEvents?=null

    //variaveis internas
    private var _player: SimpleExoPlayer? = null
    private var _uriString = uriString
    private var _playWhenReady = playWhenReady
    private var _resumePosition: Long  = resumePosition
   private var _resumeWindow = 0


    //factory
    companion object {

        fun newInstance(uriString:String = "",resumePosition: Long = 0,playWhenReady:Boolean = true): WiserPlayerFragment {
            return WiserPlayerFragment(uriString,resumePosition,playWhenReady)
        }

        fun newInstance(): WiserPlayerFragment {
            return WiserPlayerFragment("",0,true)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // Retain this fragment across configuration changes.
        retainInstance = true

    }

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_wiserplayer, container, false)
        initializePlayer(view.playerView)
        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
    }

    override fun onStart() {
        super.onStart()
    }

    override fun onPause() {
        super.onPause()
        releasePlayer()
    }

    override fun onResume() {
        super.onResume()
        if (_player == null) {
            initializePlayer(playerView)
        }
    }

    override fun onStop() {
        super.onStop()
        releasePlayer()
    }


    override fun onDestroy() {
        super.onDestroy()
        releasePlayer()
    }

    override fun onDetach() {
        super.onDetach()
        releasePlayer()
    }



    /**
     * Configura o player caso nao tenha sido usado o contrutor pra isso
     */
    fun SetupPlayer(uriString:String = "",resumePosition: Long = 0,playWhenReady:Boolean = true) {

        if (!uriString.isNullOrEmpty())
            _uriString = uriString

        if (resumePosition > 0)
            _resumePosition = resumePosition

        _playWhenReady = playWhenReady
    }


    fun Play() {
        _playWhenReady = true
        _player?.playWhenReady = _playWhenReady;
    }

    fun Pause() {
        _playWhenReady = false;
        _player?.playWhenReady = _playWhenReady;
    }


    /**
     * Inicializa os dados do player
     */
    private fun initializePlayer(playerViewReference: PlayerView?) {

        if (_uriString.isNullOrEmpty())
            return

        //val videoTrackSelectionFactory: TrackSelection.Factory = AdaptiveTrackSelection.Factory()

        val DEFAULT_COOKIE_MANAGER: CookieManager = CookieManager()
            DEFAULT_COOKIE_MANAGER.setCookiePolicy(CookiePolicy.ACCEPT_ORIGINAL_SERVER)

        if (CookieHandler.getDefault() !== DEFAULT_COOKIE_MANAGER) {
            CookieHandler.setDefault(DEFAULT_COOKIE_MANAGER)
        }

        context?.let{
            _player = SimpleExoPlayer.Builder(it).build()
        }

        //Set media controller
        playerViewReference?.useController = true
        playerViewReference?.requestFocus()
        playerViewReference?.player = _player

        val videoSource: MediaSource =  MediaSourceBuilder().build(context, _uriString)
        val haveResumePosition = _resumePosition > 0
        if (haveResumePosition) {
            _player?.seekTo(_resumePosition * 1000)
        }

        _player?.prepare(videoSource, !haveResumePosition, false)
        _player?.playWhenReady = _playWhenReady


        //configura os eventos do player que serao expostos pela interface IPlayerEvents
        _player?.addListener(object : Player.EventListener {

            override fun onTimelineChanged(timeline: Timeline, reason: Int) {
                //super.onTimelineChanged(timeline, reason)
                PlayerEventsListener?.onTimelineChanged(timeline.toString(), reason)
            }

            override fun onIsPlayingChanged(isPlaying: Boolean) {
                PlayerEventsListener?.onIsPlayingChanged(isPlaying)
            }

            override fun onTracksChanged(trackGroups: TrackGroupArray, trackSelections: TrackSelectionArray) {

            }

            override fun onPlayerError(error: ExoPlaybackException) {
                val errorMessage: String = if (error.message != null) "" else error.message!!
                val errorstackTrace: String = if (error.stackTrace != null) "" else error.stackTrace!!
                if (!errorMessage.isNullOrBlank() && !errorstackTrace.isNullOrBlank())
                    PlayerEventsListener?.onPlayerError(errorMessage, errorstackTrace)
                clearResumePosition()
            }
        })
    }





    private fun releasePlayer() {


        updateResumePosition();
        PlayerEventsListener?.onPlayerBeforeRelease()
        _player?.stop()
        _player?.release()
        _player = null
    }

    /**
     * Retorna a posição atual em segundos.
     * @return
     */
    fun getCurrentSeconds(): Int {
        _player?.let {
            return (it.getCurrentPosition() / 1000).toInt()
        }

        return 0
    }

    /**
     * Toca uma posição especifica do video
     * @param seconds
     */
    fun seek(seconds: Int) {
        this._player?.seekTo((seconds * 1000).toLong())
    }

    var isPausedByCall:Boolean = false
    fun setAudioFocusState(state:Int) {

        _player?.let{
            if (state <=0 && it.isPlaying()) {
                it.playWhenReady = false //pause
                isPausedByCall = true
                //Log.e("AUDIO_MANAGER", "paused")
            } else
                if (state > 0 && !it.isPlaying() && isPausedByCall) {
                    //Log.e("AUDIO_MANAGER", "started")
                    isPausedByCall = false
                    it.playWhenReady = true
                }
        }


    }

    private fun updateResumePosition() {
        _player?.let{
            _playWhenReady = it.playWhenReady
            _resumeWindow = it.getCurrentWindowIndex()
            _resumePosition = if (it.isCurrentWindowSeekable()) Math.max(0, getCurrentSeconds()).toLong() else C.TIME_UNSET
        }
    }

    private fun clearResumePosition() {
        _resumeWindow = C.INDEX_UNSET
        _resumePosition = C.TIME_UNSET
    }







}