package br.com.wisereducacao.vvsplayer

import com.google.android.exoplayer2.Timeline

interface IPlayerEvents {
    enum class ERROR_TYPE {
        TIMEOUT, GENERIC
    }

    fun onPlayerError(errorMessage:String,errorStackTrace:String)
    fun onPlayerComplete()
    fun onPlayerReachedTime(seconds: Int)
    fun onPlayerBeforeRelease()
    fun onIsPlayingChanged(isPlaying: Boolean)
    fun onTimelineChanged(timeline: String,reason: Int)
}